#!/usr/bin/env python
import os

import rabbitpy
import django


os.sys.path.insert(0, os.path.abspath('.'))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'conf.settings')
django.setup()

from django.conf import settings  # noqa: E402

QUEUE_NAME = 'mqtt-messages'
EXCHANGE_NAME = 'amq.topic'


if __name__ == '__main__':
    with rabbitpy.Connection(settings.MQ_URL) as conn:
        with conn.channel() as channel:
            queue = rabbitpy.Queue(channel, QUEUE_NAME)
            queue.declare()
            queue.bind(EXCHANGE_NAME, 'sensors.#')

            for msg in queue:
                msg.ack()
                parts = msg.routing_key.split('.')
                device = parts[1]
                # TODO: найти устройство, найти в нем датчик, записать измерение
                print('key', msg.routing_key, 'body', msg.body)
                reply = rabbitpy.Message(channel, msg.body)
                reply.publish(EXCHANGE_NAME, '.'.join(['devices', device]))
