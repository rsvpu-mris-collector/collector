from autocompletefilter.admin import AutocompleteFilterMixin
from autocompletefilter.filters import AutocompleteListFilter
from django.contrib import admin

from .models import Device
from .models import Location
from .models import Measurement
from .models import MeasurementLimit
from .models import MeasurementType
from .models import Sensor
from .models import Unit


class DeviceAdminTabular(admin.TabularInline):
    model = Device
    show_change_link = True
    can_delete = False
    extra = 0
    exclude = ('description', 'sensors')
    classes = ('collapse',)


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('id', 'name')
    inlines = (DeviceAdminTabular,)


@admin.register(Device)
class DeviceAdmin(AutocompleteFilterMixin, admin.ModelAdmin):
    list_display = ('id', 'name', 'type', 'serial', 'mac', 'api_protocol', 'ip', 'api_port',
                    'api_prefix', 'location', 'last_response_time')
    list_select_related = ('location',)
    search_fields = ('id', 'name', 'serial', 'mac', 'ip')
    list_filter = (
        ('location', AutocompleteListFilter),
        ('sensors', AutocompleteListFilter),
        ('sensors__unit', AutocompleteListFilter),
        ('sensors__measurement_type', AutocompleteListFilter),
    )
    filter_horizontal = ('sensors',)
    fieldsets = (
        (None, {
            'fields': (('type', 'name'),
                       'description',
                       ('serial', 'mac'),
                       ('location', 'last_response_time'),
                       ('ip', 'api_port', 'api_prefix')),
        }),
        ('Датчики', {
            'fields': ('sensors',),
            'classes': ('collapse',),
        }),
    )


@admin.register(Sensor)
class SensorAdmin(admin.ModelAdmin):
    list_display = ('id', 'model', 'measurement_type', 'unit')
    list_select_related = ('measurement_type', 'unit')


@admin.register(Unit)
class UnitAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description')
    search_fields = ('id', 'name')


@admin.register(MeasurementType)
class MeasurementTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'code_name')
    search_fields = ('id', 'name', 'code_name')


@admin.register(Measurement)
class MeasurementAdmin(AutocompleteFilterMixin, admin.ModelAdmin):
    list_display = ('id', 'datetime', 'device', 'sensor', 'location', 'value_number', 'value_str')
    list_select_related = ('device', 'sensor', 'location')
    search_fields = ('id', 'datetime', 'device__name', 'location__name', 'sensor__model',
                     'sensor__measurement_type__name')
    date_hierarchy = 'datetime'
    list_filter = (
        ('device', AutocompleteListFilter),
        ('sensor', AutocompleteListFilter),
        ('sensor__measurement_type', AutocompleteListFilter),
        ('location', AutocompleteListFilter),
        'device__type',
    )
    readonly_fields = ('datetime',)


@admin.register(MeasurementLimit)
class MeasurementLimitAdmin(AutocompleteFilterMixin, admin.ModelAdmin):
    list_display = ('id', 'measurement_type', 'unit', 'soft_min', 'soft_max', 'hard_min',
                    'hard_max')
    list_select_related = ('measurement_type', 'unit')
    search_fields = ('id', 'measurement_type__name', 'unit__name', 'soft_max', 'hard_min',
                     'hard_max')
    list_filter = (
        ('measurement_type', AutocompleteListFilter),
        ('unit', AutocompleteListFilter),
    )
