import requests

from django.utils import timezone

from conf import celery_app
from main.models import Device, Measurement


@celery_app.task
def collect():
    qs = Device.objects.exclude(ip=None)

    for device in qs:
        try:
            resp = requests.get(f'{device.api_protocol}://{device.ip}:{device.api_port}'
                                f'{device.api_prefix}/measures', timeout=30)
        except requests.exceptions.ConnectTimeout:
            continue
        if not resp.ok:
            continue

        data = resp.json()
        dt = timezone.now()

        device.last_response_time = dt
        device.save(update_fields=('last_response_time',))

        for code, value in data.items():
            sensor = device.sensors.get(measurement_type__code_name=code)
            Measurement.objects.create(device=device, datetime=dt, value_number=value,
                                       location=device.location, sensor=sensor)
