from django.db import models


class Location(models.Model):
    name = models.CharField(
        verbose_name='Имя', max_length=256,
    )
    description = models.TextField(
        verbose_name='Описание', blank=True,
    )
    limits = models.ManyToManyField(
        verbose_name='Пределы показаний', to='MeasurementLimit',
        blank=True,
    )

    class Meta:
        verbose_name = 'Местонахождение'
        verbose_name_plural = 'Местонахождения'

    def __str__(self):
        return self.name


class Device(models.Model):
    ESP = 1
    ORANGE = 2
    CAMERA = 3
    _type_choices = (
        (ESP, 'ESP'),
        (ORANGE, 'ORANGE'),
        (CAMERA, 'CAMERA'),
    )

    HTTP = 'http'
    HTTPS = 'https'
    _protocol_choices = (
        (HTTP, 'http'),
        (HTTPS, 'https'),
    )

    name = models.CharField(
        verbose_name='Имя', max_length=256,
    )
    description = models.TextField(
        verbose_name='Описание', blank=True,
    )
    type = models.PositiveIntegerField(  # noqa: A003
        verbose_name='Тип', choices=_type_choices
    )
    serial = models.CharField(
        verbose_name='Серийный номер', max_length=100, null=True, blank=True,
    )
    mac = models.CharField(
        verbose_name='MAC адрес', max_length=50, null=True, blank=True,
    )
    ip = models.GenericIPAddressField(
        verbose_name='IP адрес', null=True, blank=True,
    )
    api_protocol = models.CharField(
        verbose_name='Протокол доступа к апи', max_length=5, choices=_protocol_choices,
        default=HTTP,
    )
    api_port = models.PositiveIntegerField(
        verbose_name='Порт апи', default=80,
    )
    api_prefix = models.CharField(
        verbose_name='Префикс адреса апи (включая /)', max_length=100, default='', blank=True,
    )
    last_response_time = models.DateTimeField(
        verbose_name='Время последнего ответа', null=True, blank=True,
    )
    location = models.ForeignKey(
        verbose_name='Местонахождение', to='Location', on_delete=models.SET_NULL,
        null=True, blank=True,
    )
    sensors = models.ManyToManyField(
        verbose_name='Датчики', to='Sensor', blank=True,
    )
    firmware = models.ForeignKey(
        verbose_name='Прошивка', to='firmware.Firmware', on_delete=models.SET_NULL,
        blank=True, null=True,
    )

    class Meta:
        verbose_name = 'Устройство'
        verbose_name_plural = 'Устройства'
        default_related_name = 'devices'

    def __str__(self):
        return self.name


class Unit(models.Model):
    name = models.CharField(
        verbose_name='Имя', max_length=100,
    )
    description = models.TextField(
        verbose_name='Описание', blank=True,
    )

    class Meta:
        verbose_name = 'Единица измерения'
        verbose_name_plural = 'Единицы измерения'

    def __str__(self):
        return self.name


class Sensor(models.Model):
    model = models.CharField(
        verbose_name='Модель', max_length=255,
    )
    measurement_type = models.ForeignKey(
        verbose_name='Тип измерения', to='MeasurementType', on_delete=models.CASCADE,
    )
    unit = models.ForeignKey(
        verbose_name='Единица измерения', to='Unit', on_delete=models.PROTECT,
    )

    class Meta:
        verbose_name = 'Датчик'
        verbose_name_plural = 'Датчики'
        default_related_name = 'sensors'

    def __str__(self):
        return f'{self.measurement_type.name} - {self.model} ({self.unit})'


class MeasurementType(models.Model):
    name = models.CharField(
        verbose_name='Имя', max_length=255,
    )
    code_name = models.CharField(
        verbose_name='Кодовое имя', max_length=255,
    )

    class Meta:
        verbose_name = 'Тип показаний'
        verbose_name_plural = 'Типы показаний'
        default_related_name = 'measurement_types'

    def __str__(self):
        return self.name


class Measurement(models.Model):
    device = models.ForeignKey(
        verbose_name='Устройство', to='main.Device', on_delete=models.CASCADE,
    )
    sensor = models.ForeignKey(
        verbose_name='Датчик', to='main.Sensor', on_delete=models.CASCADE,
    )
    location = models.ForeignKey(
        verbose_name='Местонахождение', to='main.Location', on_delete=models.CASCADE,
    )
    datetime = models.DateTimeField(
        verbose_name='Дата-время', auto_now_add=True,
    )
    value_number = models.FloatField(
        verbose_name='Значение (число)', null=True, blank=True,
    )
    value_str = models.CharField(
        verbose_name='Значение (строка)', max_length=255, null=True, blank=True,
    )

    class Meta:
        verbose_name = 'Показание датчика'
        verbose_name_plural = 'Показания датчиков'
        default_related_name = 'measurements'


class MeasurementLimit(models.Model):
    measurement_type = models.ForeignKey(
        verbose_name='Тип показаний', to='MeasurementType',
        on_delete=models.CASCADE,
    )
    unit = models.ForeignKey(
        verbose_name='Единица измерения', to='Unit', on_delete=models.PROTECT,
    )
    soft_min = models.FloatField(
        verbose_name='Мягкий минимум', null=True, blank=True,
    )
    soft_max = models.FloatField(
        verbose_name='Мягкий максимум', null=True, blank=True,
    )
    hard_min = models.FloatField(
        verbose_name='Жесткий минимум', null=True, blank=True,
    )
    hard_max = models.FloatField(
        verbose_name='Жесткий максимум', null=True, blank=True,
    )

    class Meta:
        verbose_name = 'Предел показаний'
        verbose_name_plural = 'Пределы показаний'
