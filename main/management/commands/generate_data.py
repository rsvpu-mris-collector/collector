from random import randrange as rnd

from django.core.management.base import BaseCommand
from django.utils import timezone

from main.models import Device, Sensor, Unit, Measurement, MeasurementType, Location


class Command(BaseCommand):
    help = 'Генерирует несколько тестовых данных - Measurement'

    def add_arguments(self, parser):
        parser.add_argument('-n', default=10, type=int, help='Количество записей')
        parser.add_argument('-d', default='esp', type=str, help='Устройство (имя)')
        parser.add_argument('-t', default='temp', type=str, help='Тип измерения')
        parser.add_argument('-u', default='градус', type=str, help='Единица измерения')
        parser.add_argument('-s', default='dht', type=str, help='Датчик')
        parser.add_argument('-l', default='0-225', type=str, help='Местонахождение')
        parser.add_argument('-b', default='10-30', type=str, help='Границы значений')

    def handle(self, *args, **options):
        count = options['n']
        start = timezone.now() - timezone.timedelta(days=1)
        device, _ = Device.objects.get_or_create(name=options['d'], defaults={'type': Device.ESP})
        unit, _ = Unit.objects.get_or_create(name=options['u'])
        mt, _ = MeasurementType.objects.get_or_create(name=options['t'], code_name=options['t'])
        sensor, _ = Sensor.objects.get_or_create(model=options['s'], unit=unit, measurement_type=mt)
        loc, _ = Location.objects.get_or_create(name=options['l'])

        v_start, v_stop = map(int, options['b'].split('-'))
        for i in range(count):
            time = start - timezone.timedelta(minutes=10 * i)
            meas = Measurement.objects.create(device=device,
                                              value_number=rnd(v_start, v_stop),  # noqa: S311
                                              location=loc, sensor=sensor)
            meas.datetime = time
            meas.save()

        self.stdout.write(self.style.SUCCESS(f'Создано {count}'))
