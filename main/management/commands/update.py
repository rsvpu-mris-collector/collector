import traceback

from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db import transaction


class Command(BaseCommand):
    help = 'атомарная миграция и статика'

    def add_arguments(self, parser):
        parser.add_argument('--no-migrate', help='мигрировать базу', action='store_true')
        parser.add_argument('--no-static', help='закинуть статику', action='store_true')

    def handle(self, *args, **options):
        if not options.get('no_migrate'):
            self.stdout.write(self.style.SUCCESS('мигрируем...'))
            with transaction.atomic():
                call_command('migrate')

        if not options.get('no_static'):
            self.stdout.write(self.style.SUCCESS('кидаем статику...'))
            try:
                call_command('collectstatic', '--noinput')
            except Exception:
                traceback.print_exc()
