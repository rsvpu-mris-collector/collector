from django.contrib import admin

from .models import GitCommit
from .models import Firmware


@admin.register(GitCommit)
class GitCommitAdmin(admin.ModelAdmin):
    list_display = ('id', 'branch', 'datetime', 'short_description', 'short_sha')
    search_fields = ('id',)


@admin.register(Firmware)
class FirmwareAdmin(admin.ModelAdmin):
    list_display = ('id', 'commit')
    list_select_related = ('commit',)
    search_fields = ('id',)
