import os

import git
from platformio.commands.run.command import cli as pio_run


class PioError(Exception):
    pass


class BuildError(PioError):
    pass


class UploadError(PioError):
    pass


def update_repo(ref_name: str = None) -> dict:
    """Переводит репозиторий в состояние {ref_name}.

    Если папка пуста, репозиторий клонируется,
    в противном случае обновляется.
    :param ref_name: Ссылка на состояние: коммит, ветка, метка.
      Если не указана, обновляется текущая ветка.
    :return: инфа о коммите: хеш, дата, ветка, сообщение.
    """
    repo_path = os.environ["PIO_PROJECT_DIR"]

    try:
        repo = git.Repo(repo_path)
    except (git.NoSuchPathError, git.InvalidGitRepositoryError):
        repo_url = os.environ["REPO_URL"]
        repo = git.Repo.clone_from(repo_url, repo_path)

    repo.git.fetch()
    if ref_name:
        repo.git.checkout(ref_name)
    else:
        repo.git.rebase()
    commit = repo.head.ref.commit
    return {
        'sha': commit.hexsha,
        'datetime': commit.commited_datetime,
        'branch': repo.active_branch.name,
        'summary': commit.summary,
        'message': commit.message,
    }


def build_firmware():
    """Собирает прошивку в файл."""
    # file - /repo/.pio/build/d1_mini/firmware.bin
    os.environ["PIO_TARGET"] = ""
    try:
        pio_run(auto_envvar_prefix="PIO")
    except SystemExit as e:
        if e.code != 0:
            raise BuildError  # TODO: собрать лог


def upload_firmware(target_ip: str):  # TODO: указывать файл
    """Загружает прошивку в устройство."""
    os.environ["PIO_TARGET"] = "upload"
    os.environ["PIO_UPLOAD_PORT"] = target_ip
    try:
        pio_run(auto_envvar_prefix="PIO")
    except SystemExit as e:
        if e.code != 0:
            raise UploadError  # TODO: собрать лог
