from django.db import models


class GitCommit(models.Model):
    sha = models.CharField(
        verbose_name='SHA коммита', max_length=20,
    )
    short_description = models.CharField(
        verbose_name='Короткое описание', max_length=256,
    )
    long_description = models.TextField(
        verbose_name='Длинное описание', default='', blank=True,
    )
    branch = models.CharField(
        verbose_name='Ветка', max_length=256,
    )
    datetime = models.DateTimeField(
        verbose_name='Дата-время',
    )

    class Meta:
        verbose_name = 'Коммит'
        verbose_name_plural = 'Коммиты'

    @property
    def short_sha(self):
        return self.sha[:8]

    def __str__(self):
        return f'Коммит ({self.id}) {self.short_sha}'


class Firmware(models.Model):
    commit = models.OneToOneField(
        verbose_name='Коммит', to='GitCommit', on_delete=models.SET_NULL,
        null=True, blank=True,
    )
    bin_file = models.FileField(
        verbose_name='BIN файл', upload_to='firmware',
    )
    comment = models.TextField(
        verbose_name='Комментарий', default='', blank=True,
    )

    class Meta:
        verbose_name = 'Прошивка'
        verbose_name_plural = 'Прошивки'

    def __str__(self):
        return f'Прошивка ({self.id})' + (f' {self.commit}' if self.commit else '')
