## INSTALL

requirements:
  - python 3.7+
  - poetry
  - postgresql
  - rabbitmq
  - grafana

  or
  - docker
  - docker-compose

**a. docker**
```bash
./dc build
./dc run --rm backend update
```

**b. w/o docker**
```bash
psql -U postgres -c "CREATE ROLE collector_user WITH LOGIN PASSWORD <password>"
createdb -U postgres -O collector_user collector
echo "DATABASE_URL=psql://collector_user:<password>@localhost:5432/collector" > conf/.env
pip install poetry
poetry install
poetry shell
./manage.py update 
```

## RUN

**a. docker**
```bash
./dc up -d
```

**b. w/o docker**
```bash
# <launch postgres>
# <launch rabbitmq>
# <launch grafana>
poetry shell
./consumer.py &
./manage.py runserver 0.0.0.0:8000 &
celery worker -A conf -l info &
celery beat -A conf -l info -S django
flower --port=5555
```
