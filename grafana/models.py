from django.db import models

__all__ = ['Panel', 'Series', 'Target', 'Dashboard']


class Dashboard(models.Model):
    # https://grafana.com/docs/grafana/latest/reference/dashboard/#json-fields
    NO = 0
    CROSSHAIRS = 1
    CROSSHAIRS_AND_TOOLTIPS = 2
    TOOLTIPS_CHOICES = (
        (NO, 'ничего'),
        (CROSSHAIRS, 'Перекрестие'),
        (CROSSHAIRS_AND_TOOLTIPS, 'Перекрестия и подсказки'),
    )

    grafana_id = models.IntegerField(
        verbose_name='Идентификатор в графане', null=True, blank=True,
    )
    uid = models.CharField(
        verbose_name='UID в графане', max_length=100, null=True, blank=True,
    )
    title = models.CharField(
        verbose_name='Заголовок', max_length=50,
    )
    editable = models.BooleanField(
        verbose_name='Редактируемая', default=False,
    )
    hide_controls = models.BooleanField(  # hideControls
        verbose_name='Скрыть элементы управления', default=False,
    )
    graph_tooltip = models.SmallIntegerField(  # graphTooltip
        verbose_name='Общие подсказки', choices=TOOLTIPS_CHOICES, default=NO,
    )

    class Meta:
        verbose_name = 'Доска'
        verbose_name_plural = 'Доски'

    def __str__(self):
        return self.title

    def save_json(self):
        import json
        from .serializers import DashboardSerializer
        data = DashboardSerializer(self).data
        json_data = json.dumps(data, ensure_ascii=False, indent=4)
        with open(f'grafana/dashboards/{self.id}.json', 'w') as f:
            f.write(json_data)


class Panel(models.Model):
    # https://github.com/grafana/grafana/blob/master/public/app/plugins/panel/graph/module.ts
    """
    // datasource name, null = default datasource
    datasource: null,
    // sets client side (flot) or native graphite png renderer (png)
    renderer: 'flot',
    yaxes: [
        {
            label: null,
            show: true,
            logBase: 1,
            min: null,
            max: null,
            format: 'short',
        },
        {
            label: null,
            show: true,
            logBase: 1,
            min: null,
            max: null,
            format: 'short',
        },
    ],
    xaxis: {
        show: true,
        mode: 'time',
        name: null,
        values: [],
        buckets: null,
    },
    yaxis: {
        align: false,
        alignLevel: null,
    },
    lines: true,
    // fill factor
    fill: 1,
    fillGradient: 0,
    linewidth: 1
    dashes: false,
    // show/hide line
    hiddenSeries: false,
    dashLength: 10,
    // length of space between two dashes
    spaceLength: 10,
    points: false,
    pointradius: 2,
    bars: false,
    stack: false,
    // stack percentage mode
    percentage: false,
    legend: {
        show: true,
        values: false,
        min: false,
        max: false,
        current: false,
        total: false,
        avg: false,
    },
    // how null points should be handled
    nullPointMode: 'null',
    // staircase line mode
    steppedLine: false,
    tooltip: {
        value_type: 'individual',
        shared: true,
        sort: 0,
    },
    // time overrides
    timeFrom: null,
    timeShift: null,
    // metric queries
    targets: [{}],
    // series color overrides
    aliasColors: {},
    // other style overrides
    seriesOverrides: [],
    thresholds: [],
    timeRegions: [],
    options: {
        dataLinks: [],
    }
    """

    GRAPH = 'graph'
    SINGLESTAT = 'singlestat'
    GAUGE = 'gauge'
    BAR_GAUGE = 'bargauge'
    TABLE = 'table'
    TEXT = 'text'
    HEATMAP = 'heatmap'
    ALERT_LIST = 'alertlist'
    DASHBOARD_LIST = 'dashboardlist'
    PLUGIN_LIST = 'pluginlist'

    TYPE_CHOICES = (
        (GRAPH, 'Граф'),
        (SINGLESTAT, 'Единичное значение'),
        (GAUGE, 'Шкала'),
        (BAR_GAUGE, 'Столбцы'),
        (TABLE, 'Таблица'),
        (TEXT, 'Текст'),
        (HEATMAP, 'Тепловая карта'),
        (ALERT_LIST, 'Список предупреждений'),
        (DASHBOARD_LIST, 'Список досок'),
        (PLUGIN_LIST, 'Список плагинов'),
    )

    COLOR_CHOICES = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
        (6, '6'),
        (7, '7'),
        (8, '8'),
        (9, '9'),
        (10, '10'),
    )

    CONNECTED = 'connected'
    NONE = 'None'
    NULL_AS_ZERO = 'null as zero'
    NULL_VALUE_CHOICES = (
        (CONNECTED, 'Connected'),
        (NONE, 'None'),
        (NULL_AS_ZERO, 'null -> 0'),
    )

    DISABLED = 0
    ASC = 1
    DESC = 2
    SORT_CHOICES = (
        (DISABLED, 'Без сортировки'),
        (ASC, 'По возрастанию'),
        (DESC, 'По убыванию'),
    )

    INDIVIDUAL = 'individual'
    CUMULATIVE = 'cumulative'
    VALUE_TYPE_CHOICES = (
        (INDIVIDUAL, 'Индивидуально'),
        (CUMULATIVE, 'Накопительно'),
    )

    AVG = 'avr'
    MIN = 'min'
    MAX = 'max'
    TOTAL = 'total'
    COUNT = 'count'
    CURRENT = 'current'
    VALUE_CHOICES = (
        (AVG, 'Среднее'),
        (MIN, 'Минимум'),
        (MAX, 'Максимум'),
        (TOTAL, 'Итог'),
        (COUNT, 'Количество'),
        (CURRENT, 'Текущее'),
    )

    SHORT = 'short'
    FORMAT_CHOICES = (
        (SHORT, 'short'),
        # https://github.com/grafana/grafana/blob/aa0982da56684bf2ad5c04fc16c7e1da43f7e05a/packages/grafana-data/src/valueFormats/categories.ts
    )

    SCALE_CHOICES = (
        (2, '2'),
        (10, '10'),
        (32, '32'),
        (1024, '1024'),
    )

    TIME = 'time'
    SERIES = 'series'
    HISTOGRAM = 'histogram'
    MODE_CHOICES = (
        (TIME, 'Время'),
        (SERIES, 'Серии'),
        (HISTOGRAM, 'Гистрограмма'),
    )

    dashboard = models.ForeignKey(
        verbose_name='Доска', to='Dashboard', on_delete=models.SET_NULL, null=True, blank=True,
    )

    # general
    type = models.CharField(
        verbose_name='Тип', max_length=20, choices=TYPE_CHOICES,
    )
    title = models.CharField(
        verbose_name='Заголовок', max_length=50,
    )
    transparent = models.BooleanField(
        verbose_name='Прозрачность', default=True,
    )

    target = models.ForeignKey(
        verbose_name='Запрос', to='Target', on_delete=models.CASCADE, null=True,
    )

    # draw modes
    bars = models.BooleanField(
        verbose_name='Столбцы', default=False,
    )
    lines = models.BooleanField(
        verbose_name='Линии', default=True,
    )
    points = models.BooleanField(
        verbose_name='Точки', default=True,
    )
    # mode options
    line_width = models.PositiveIntegerField(  # linewidth
        verbose_name='Толщина линии', default=2,
    )
    point_radius = models.PositiveIntegerField(  # pointradius
        verbose_name='Радиус точки', default=3,
    )
    fill = models.PositiveIntegerField(
        verbose_name='Заливка', choices=COLOR_CHOICES,
    )
    gradient = models.PositiveIntegerField(  # fillGradient
        verbose_name='Градиент (насыщенность)', choices=COLOR_CHOICES,
    )
    stepped_line = models.BooleanField(  # steppedLine
        verbose_name='Stepped line', default=False,
    )

    # hover tooltip
    tooltip_shared = models.BooleanField(
        verbose_name='Mode', help_text='single - false или all series - true', default=True,
    )
    tooltip_sort = models.PositiveIntegerField(
        verbose_name='Сортировка', choices=SORT_CHOICES, default=DISABLED,
    )
    tooltip_value_type = models.CharField(  # при stack=true
        verbose_name='value type', max_length=10, choices=VALUE_TYPE_CHOICES, default=INDIVIDUAL,
    )

    # stacking & null value
    stack = models.BooleanField(
        verbose_name='Стек', default=False,
    )
    null_point_mode = models.CharField(
        verbose_name='Отображение null', max_length=12, choices=NULL_VALUE_CHOICES, default=NONE,
    )
    percentage = models.BooleanField(  # при stack=true
        verbose_name='Percent', default=False,
    )

    series = models.ManyToManyField(
        verbose_name='Серии', to='Series', blank=True,
    )

    # axes
    # yaxes = [left y, right y]
    left_y_show = models.BooleanField(
        verbose_name='Левая ось Y - Показывать', default=True,
    )
    left_y_format = models.CharField(
        verbose_name='Левая ось Y - Единица измерения', max_length=20, choices=FORMAT_CHOICES,
        default=SHORT,
    )
    left_y_scale = models.PositiveIntegerField(  # logBase
        verbose_name='Левая ось Y - Масштаб (логарифмический)', choices=SCALE_CHOICES, default=1,
    )
    left_y_min = models.PositiveIntegerField(
        verbose_name='Левая ось Y - Минимум', null=True, blank=True,
    )
    left_y_max = models.PositiveIntegerField(
        verbose_name='Левая ось Y - Максимум', null=True, blank=True,
    )
    left_y_label = models.CharField(
        verbose_name='Левая ось Y - Подпись', max_length=50, default='',
    )
    right_y_show = models.BooleanField(
        verbose_name='Правая ось Y - Показывать', default=True,
    )
    right_y_format = models.CharField(
        verbose_name='Правая ось Y - Единица измерения', max_length=20, choices=FORMAT_CHOICES,
        default=SHORT,
    )
    right_y_scale = models.PositiveIntegerField(  # logBase
        verbose_name='Правая ось Y - Масштаб (логарифмический)', choices=SCALE_CHOICES, default=1,
    )
    right_y_min = models.PositiveIntegerField(
        verbose_name='Правая ось Y - Минимум', null=True, blank=True,
    )
    right_y_max = models.PositiveIntegerField(
        verbose_name='Правая ось Y - Максимум', null=True, blank=True,
    )
    right_y_label = models.CharField(
        verbose_name='Правая ось Y - Подпись', max_length=50, default='',
    )

    xaxis_show = models.BooleanField(
        verbose_name='Оси X - Показывать', default=True,
    )
    xaxis_mode = models.CharField(
        verbose_name='Оси X - Режим', max_length=9, choices=MODE_CHOICES, default=TIME,
    )
    xaxis_value = models.CharField(  # для mode=series, values=[value] если не None
        verbose_name='Оси X - Value', max_length=7, choices=VALUE_CHOICES, null=True, blank=True,
    )
    xaxis_buckets = models.PositiveIntegerField(  # для mode=histogram
        verbose_name='Оси X - Ведра', null=True, blank=True,
    )
    xaxis_min = models.PositiveIntegerField(  # для mode=histogram
        verbose_name='Оси X - Минимум', null=True, blank=True,
    )
    xaxis_max = models.PositiveIntegerField(  # для mode=histogram
        verbose_name='Оси X - Максимум', null=True, blank=True,
    )

    yaxis_align = models.BooleanField(
        verbose_name='Ось Y - выравнивание', default=False,
    )
    yaxis_align_level = models.PositiveIntegerField(  # alignLevel
        verbose_name='Ось Y - уровень', null=True, blank=True,
    )

    legend_show = models.BooleanField(
        verbose_name='Показывать', default=True,
    )
    legend_align_as_table = models.BooleanField(  # alignAsTable
        verbose_name='Как таблица', default=True,
    )
    legend_right_side = models.BooleanField(  # rightSide
        verbose_name='Прибить к правой стороне', default=False,
    )
    legend_side_width = models.PositiveIntegerField(  # sideWidth, для rightSide
        verbose_name='Ширина панели', null=True, blank=True,
    )
    legend_min = models.BooleanField(
        verbose_name='Минимум', default=True,
    )
    legend_max = models.BooleanField(
        verbose_name='Максимум', default=True,
    )
    legend_avg = models.BooleanField(
        verbose_name='Среднее', default=True,
    )
    legend_current = models.BooleanField(
        verbose_name='Текущее', default=True,
    )
    legend_total = models.BooleanField(
        verbose_name='Итог', default=False,
    )
    legend_hide_empty = models.BooleanField(  # hideEmpty
        verbose_name='Скрывать null', default=False,
    )
    legend_hide_zero = models.BooleanField(  # hideZero
        verbose_name='Скрывать нули', default=False,
    )
    legend_values = models.BooleanField(
        verbose_name='values', default=True,
    )
    legend_decimals = models.PositiveIntegerField(
        verbose_name='Разряды', null=True, blank=True,
    )

    dashes = models.BooleanField(
        verbose_name='Пунктирные линии', default=False,
    )
    dash_length = models.PositiveIntegerField(  # dashLength
        verbose_name='Длина пунктира', default=10,
    )
    space_length = models.PositiveIntegerField(  # spaceLength
        verbose_name='Расстояние между пунктиром', default=10,
    )
    hidden_series = models.BooleanField(  # hiddenSeries
        verbose_name='Скрыть серии', default=False,
    )

    grid_pos_x = models.PositiveIntegerField(
        verbose_name='Позиция - Х', default=0,
    )
    grid_pos_y = models.PositiveIntegerField(
        verbose_name='Позиция - Y', default=0,
    )
    grid_pos_w = models.PositiveIntegerField(
        verbose_name='Позиция - W', default=24,
    )
    grid_pos_h = models.PositiveIntegerField(
        verbose_name='Позиция - H', default=7,
    )

    class Meta:
        verbose_name = 'Панель'
        verbose_name_plural = 'Панели'
        default_related_name = 'panels'

    def __str__(self):
        return self.title


class Series(models.Model):
    alias = models.CharField(
        verbose_name='Значение поля', max_length=100,
    )
    color = models.CharField(
        verbose_name='Цвет (hex, с #)', max_length=7,
    )

    class Meta:
        verbose_name = 'Серия'
        verbose_name_plural = 'Серии'
        default_related_name = 'series'

    def __str__(self):
        return f'alias={self.alias}, color={self.color}'


class Target(models.Model):
    location = models.ForeignKey(
        verbose_name='Местонахождение', to='main.Location', on_delete=models.CASCADE,
    )
    measurement_type = models.ForeignKey(
        verbose_name='Тип измерения', to='main.MeasurementType', on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = 'Запрос'
        verbose_name_plural = 'Запросы'
        default_related_name = 'targets'

    def __str__(self):
        return f'Запрос {self.id}'
