from rest_framework import serializers

from .models import Panel, Series, Target, Dashboard


class SeriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Series
        exclude = ('id',)


class TargetSerializer(serializers.ModelSerializer):
    format = serializers.SerializerMethodField()  # noqa: A003
    groups = serializers.SerializerMethodField()
    hide = serializers.SerializerMethodField()
    metricColumn = serializers.SerializerMethodField()
    rawQuery = serializers.SerializerMethodField()
    refId = serializers.SerializerMethodField()
    select = serializers.SerializerMethodField()
    table = serializers.SerializerMethodField()
    timeColumn = serializers.SerializerMethodField()
    timeColumnType = serializers.SerializerMethodField()
    where = serializers.SerializerMethodField()
    rawSql = serializers.SerializerMethodField()

    class Meta:
        model = Target
        exclude = ('id', 'measurement_type', 'location')

    def get_format(self, obj):
        return 'time_series'

    def get_groups(self, obj):
        return []

    def get_hide(self, obj):
        return False

    def get_metricColumn(self, obj):
        return 'metric'

    def get_rawQuery(self, obj):
        return True

    def get_refId(self, obj):
        return 'A'

    def get_select(self, obj):
        return [
            [
                {
                    'params': ['value_number'],
                    'type': 'column'
                }
            ]
        ]

    def get_table(self, obj):
        return 'main_measurement'

    def get_timeColumn(self, obj):
        return 'datetime'

    def get_timeColumnType(self, obj):
        return 'timestamp'

    def get_where(self, obj):
        return [
            {
                "name": "$__timeFilter",
                "params": [],
                "type": "macro"
            }
        ]

    def get_rawSql(self, obj: Target):
        return ("SELECT"  # noqa
                " $__time(datetime),"
                f" '{obj.measurement_type.name}' AS metric,"
                " value_number\n"
                "FROM main_measurement\n"
                "INNER JOIN main_sensor ON main_measurement.sensor_id = main_sensor.id\n"
                f"WHERE main_measurement.location_id = {obj.location_id} AND"
                f" main_sensor.measurement_type_id = {obj.measurement_type_id}"
                " AND $__timeFilter(datetime)\n"
                "ORDER BY datetime")


class PanelSerializer(serializers.ModelSerializer):
    yaxes = serializers.SerializerMethodField()
    xaxis = serializers.SerializerMethodField()
    yaxis = serializers.SerializerMethodField()
    fillGradient = serializers.IntegerField(source='gradient')
    linewidth = serializers.IntegerField(source='line_width')
    hiddenSeries = serializers.BooleanField(source='hidden_series')
    dashLength = serializers.IntegerField(source='dash_length')
    spaceLength = serializers.IntegerField(source='space_length')
    pointradius = serializers.IntegerField(source='point_radius')
    legend = serializers.SerializerMethodField()
    nullPointMode = serializers.CharField(source='null_point_mode')
    steppedLine = serializers.BooleanField(source='stepped_line')
    tooltip = serializers.SerializerMethodField()
    targets = serializers.SerializerMethodField()
    seriesOverrides = SeriesSerializer(source='series', many=True)
    gridPos = serializers.SerializerMethodField()

    class Meta:
        model = Panel
        fields = ('id', 'type', 'yaxes', 'xaxis', 'yaxis', 'lines', 'fill', 'fillGradient',
                  'linewidth', 'dashes', 'hiddenSeries', 'dashLength', 'spaceLength',
                  'points', 'pointradius', 'bars', 'stack', 'percentage', 'legend',
                  'nullPointMode', 'steppedLine', 'tooltip', 'targets', 'seriesOverrides',
                  'gridPos', 'title', 'transparent')

    def get_yaxes(self, obj):
        return [
            {
                'show': obj.left_y_show,
                'label': obj.left_y_label,
                'logBase': obj.left_y_scale,
                'min': obj.left_y_min,
                'max': obj.left_y_max,
                'format': obj.left_y_format,
            },
            {
                'show': obj.right_y_show,
                'label': obj.right_y_label,
                'logBase': obj.right_y_scale,
                'min': obj.right_y_min,
                'max': obj.right_y_max,
                'format': obj.right_y_format,
            },
        ]

    def get_xaxis(self, obj):
        return {
            'show': obj.xaxis_show,
            'mode': obj.xaxis_mode,
            'name': None,
            'values': [obj.xaxis_value] if obj.xaxis_value else [],
            'buckets': obj.xaxis_buckets,
            'min': obj.xaxis_min,
            'max': obj.xaxis_max,
        }

    def get_yaxis(self, obj):
        return {
            'align': obj.yaxis_align,
            'alignLevel': obj.yaxis_align_level,
        }

    def get_legend(self, obj):
        return {
            'show': obj.legend_show,
            'alignAsTable': obj.legend_align_as_table,
            'rightSide': obj.legend_right_side,
            'values': obj.legend_values,
            'min': obj.legend_max,
            'max': obj.legend_min,
            'current': obj.legend_current,
            'total': obj.legend_total,
            'avg': obj.legend_avg,
            'hideEmpty': obj.legend_hide_empty,
            'hideZero': obj.legend_hide_zero,
            'decimals': obj.legend_decimals,
        }

    def get_tooltip(self, obj):
        return {
            'value_type': obj.tooltip_value_type,
            'shared': obj.tooltip_shared,
            'sort': obj.tooltip_sort,
        }

    def get_targets(self, obj):
        return [TargetSerializer(obj.target).data]

    def get_gridPos(self, obj):
        return {
            'x': obj.grid_pos_x,
            'y': obj.grid_pos_y,
            'w': obj.grid_pos_w,
            'h': obj.grid_pos_h,
        }


class DashboardSerializer(serializers.ModelSerializer):
    panels = PanelSerializer(many=True)
    schemaVersion = serializers.SerializerMethodField()
    hideControls = serializers.BooleanField(source='hide_controls')
    graphTooltip = serializers.IntegerField(source='graph_tooltip')

    class Meta:
        model = Dashboard
        exclude = ('hide_controls', 'graph_tooltip')

    def get_schemaVersion(self, obj):
        return 19
