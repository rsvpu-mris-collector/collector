from django.contrib import admin
from django.forms import ModelForm
from django.forms.widgets import TextInput
from django_object_actions import DjangoObjectActions

from .models import (
    Panel, Series, Target, Dashboard,
)


class SeriesForm(ModelForm):
    class Meta:
        model = Series
        fields = '__all__'
        widgets = {
            'color': TextInput(attrs={'type': 'color'}),
        }


class PanelSeriesInlineAdmin(admin.TabularInline):
    model = Panel.series.through
    verbose_name = 'Серия'
    verbose_name_plural = 'Серии'
    extra = 1


@admin.register(Series)
class SeriesAdmin(admin.ModelAdmin):
    form = SeriesForm
    search_fields = ('id',)


@admin.register(Panel)
class PanelAdmin(admin.ModelAdmin):
    search_fields = ('id', 'title')
    fieldsets = (
        ('Основное', {
            'fields': ('dashboard', 'type', 'title', 'transparent', ('grid_pos_x', 'grid_pos_y'),
                       ('grid_pos_w', 'grid_pos_h')),
        }),
        ('Отображение', {
            'fields': (
                ('bars', 'lines', 'points'),
                'fill', 'gradient', 'line_width',
                'stepped_line', 'point_radius', 'dashes', 'dash_length', 'space_length',
                'stack', 'null_point_mode', 'percentage',
            ),
            'classes': ('collapse',),
        }),
        ('Оси', {
            'fields': (
                'left_y_show', 'left_y_format', 'left_y_scale', 'left_y_min', 'left_y_max',
                'left_y_label',
                'right_y_show', 'right_y_format', 'right_y_scale', 'right_y_min', 'right_y_max',
                'right_y_label',
                'xaxis_show', 'xaxis_mode', 'xaxis_value', 'xaxis_buckets', 'xaxis_min',
                'xaxis_max',
                'yaxis_align', 'yaxis_align_level',
            ),
            'classes': ('collapse',),
        }),
        ('Всплывающая подсказа', {
            'fields': ('tooltip_shared', 'tooltip_sort', 'tooltip_value_type'),
        }),
        ('Легенда', {
            'fields': (
                ('legend_show', 'legend_align_as_table', 'legend_right_side'),
                'legend_side_width',
                ('legend_min', 'legend_max', 'legend_avg', 'legend_current', 'legend_total'),
                'legend_decimals',
                ('legend_hide_empty', 'legend_hide_zero'),
            ),
            'classes': ('collapse',),
        }),
        ('Данные', {
            'fields': ('target',)
        }),
    )
    inlines = (PanelSeriesInlineAdmin,)
    save_as = True


@admin.register(Target)
class TargetAdmin(admin.ModelAdmin):
    search_fields = ('id',)


class PanelInlineAdmin(admin.TabularInline):
    model = Panel
    show_change_link = True
    extra = 1
    fields = ('id', 'type', 'title')


@admin.register(Dashboard)
class DashboardAdmin(DjangoObjectActions,
                     admin.ModelAdmin):
    search_fields = ('id',)
    change_actions = ('save_json',)

    def save_json(self, request, obj: Dashboard):
        obj.save_json()
    save_json.label = 'Сохранить в Grafana'
    inlines = (PanelInlineAdmin,)
