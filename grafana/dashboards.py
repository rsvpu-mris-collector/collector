# flake8: noqa

legend = {
    # options
    "show": True,
    "alignAsTable": True,
    "rightSide": False,
    # values
    "min": True,
    "max": True,
    "avg": True,
    "current": True,
    "total": False,
    # hide series
    "hideEmpty": False,
    "hideZero": False,

    "values": True,
}
tooltip = {
    "shared": True,
    "sort": 0,
    "value_type": "individual"
}
thresholds = [
    {
        "op": "gt",
        "value": 20,
        "colorMode": "critical",
        "fill": True,
        "line": True,
        "yaxis": "left",
    },
    {
        "op": "lt",
        "value": 10,
        "colorMode": "critical",
        "fill": True,
        "line": True,
        "yaxis": "left",
    }
]

yaxes = [
    {
        "show": True,
        "format": "celsius",
        "logBase": 1,  # linear
        "min": "0",
        "max": None,
        "label": "",
    },
    {
        "show": False,
        "format": "celsius",
        "logBase": 1,
        "min": None,
        "max": None,
        "label": "",
    }
]
xaxis = {
    "show": True,
    "mode": "time",
    "buckets": None,
    "name": None,
    "values": [],
    "min": None,
    "max": None,
}
yaxis = {
    "align": False,
    "alignLevel": None
}

series_overrides = [
    {
        "alias": "Температура",
        "color": "#8AB8FF"
    }
]
grid_pos = {
    "h": 7,
    "w": 12,
    "x": 0,
    "y": 0
}
targets = [
    {
        "format": "time_series",
        "group": [],
        "hide": False,
        "metricColumn": "metric",
        "rawQuery": True,
        "rawSql": ("SELECT"
                   " $__time(datetime),"
                   " main_measurementtype.name AS metric,"
                   " value as value\n"
                   "FROM main_measurement\n"
                   "INNER JOIN main_sensor ON main_measurement.sensor_id = main_sensor.id\n"
                   "INNER JOIN main_location ON main_measurement.location_id = main_location.id\n"
                   "INNER JOIN main_measurementtype ON main_sensor.measurement_type_id = main_measurementtype.id\n"
                   "WHERE main_location.name = '0-225' AND main_measurementtype.name = 'Температура'"
                   " AND $__timeFilter(datetime)\n"
                   "ORDER BY datetime"),
        "refId": "A",
        "select": [
            [
                {
                    "params": [
                        "value"
                    ],
                    "type": "column"
                }
            ]
        ],
        "table": "main_measurement",
        "timeColumn": "datetime",
        "timeColumnType": "timestamp",
        "where": [
            {
                "name": "$__timeFilter",
                "params": [],
                "type": "macro"
            }
        ]
    }
]

panel_json_example = {
    "id": 2,
    "type": "graph",
    "title": "Температура",
    "transparent": True,

    "aliasColors": {},

    # query
    "targets": targets,
    "timeFrom": "1m",
    "timeShift": None,

    # draw modes
    "bars": False,
    "lines": True,
    "points": True,

    # mode options
    "fill": 9,
    "fillGradient": 10,
    "linewidth": 2,
    "steppedLine": False,
    "pointradius": 3,

    # hover tooltip
    "tooltip": tooltip,

    # stacking & null value
    "stack": False,
    "NonePointMode": "None",
    "percentage": False,

    "seriesOverrides": series_overrides,

    # axes
    # left y, right y
    "yaxes": yaxes,
    "xaxis": xaxis,
    "yaxis": yaxis,

    "legend": legend,
    "decimals": None,

    "thresholds": thresholds,

    "options": {
        "dataLinks": []
    },

    # хз
    "dashes": False,
    "dashLength": 10,
    "renderer": "flot",
    "spaceLength": 10,

    "timeRegions": [],

    "gridPos": grid_pos,
}
