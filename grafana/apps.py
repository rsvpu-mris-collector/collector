from django.apps import AppConfig


class GrafanaConfig(AppConfig):
    name = 'grafana'
