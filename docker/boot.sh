#!/bin/bash

set -e

/wait-for-it.sh $DB_WAIT
/wait-for-it.sh $MQ_WAIT

if [[ "$1" = 'update' ]]; then
  set -- python manage.py update $2 $3
elif [[ "$1" = 'server' ]]; then
  if [[ "$DEBUG" != '' ]]; then
    set -- python manage.py runserver_plus 0.0.0.0:8000
  else
    set -- gunicorn conf.wsgi -b 0.0.0.0:8000
  fi
elif [[ "$1" = 'consumer' ]]; then
  set -- python consumer.py
elif [[ "$1" = 'worker' ]]; then
  set -- celery worker -A conf -l info
elif [[ "$1" = 'scheduler' ]]; then
  set -- celery beat -A conf -l info -S django
elif [[ "$1" = 'flower' ]]; then
  set -- flower --port=5555
fi

exec "$@"
